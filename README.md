# Interview Exercise #

exercise-a1
Position: Front-end Designer/Developer
Objective: Demonstrate front-end coding experience using Visual Studio and Git.
Estimated completion time: 30-60 min

### Your Task ###

A junior developer created a HTML landing page for one of our holidays. The content is OK, but the code and folders structure can be improved. Your task is to make the improvements you consider relevant and explain why. You don’t need to make this page looks better, just focus on the code. After all the improvements, the page should looks-like the same.

The project is backed by a Git repository in Bitbucket. You should clone the existing repository and create your own branch from master, before start working on your changes. After you complete your changes in your own branch, make a pull request to the master branch in the original repository.

### Copyright ###

All the content used in this exercise belongs to Live Holidays Ltd, and cannot be used without a written permission from Live Holidays Ltd.